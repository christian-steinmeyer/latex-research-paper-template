# LaTeX Paper Template

For examples, have a look at [the wiki](../../wikis/Examples).

### Documentation

The template itself is documented inside the files. 
For a good start, have a look at the [main file](main.tex).

### Overleaf

This template is optimized for the use with [Overleaf](https://overleaf.com).


### Submission

If writing a paper for an organization, 
and that organization prefers a single LaTeX file, 
this can be achived with a simple 
[python script](https://gitlab.com/christian-steinmeyer/flatten-latex-or-flattex).

# Targeted Versions
There are targeted versions for different audiences.
They are to be maintained in separate branches.
Any changes that are not specific to them are to be made in the master branch
and then merged into the other branches as well.
Any changes that **are** specific to them are to be made only in that branch.

### IEEE
This version adheres to the layout and format specifications for 
[IEEE](https://www.ieee.org/conferences/publishing/templates.html) (state of 2019-12).

# Participation
You are welcome to participate in the improvement of this template.
Be it through raising issues,
creating feature branch based pull requests,
or adding additional targeted versions.

Generally, we adhere to a strict [gitlab workflow](https://gitlab.cc-asp.fraunhofer.de/bioinformatik-item/gitlab-workflow).
However, as this repository contains multiple branches 
that are maintained over longer period of time,
some changes need to be integrated in a different matter.

All branches with targeted versions are based on the `master` branch.
Through meeting target-specific requirements, the diverge from that state,
and cannot be simply merged without introducing conflicting states.
Thus, the workflow is adapted in the following ways:
- Changes specific to a targeted version are introduced via feature branches and 
merge conflicts to those branches as described in the workflow
- General changes that concern all (or multiple) versions of the template
are introduced in the `master` branch in the regular fashion. 
Those changes are then propagated to the other versions through [cherry-picking](https://docs.gitlab.com/ee/user/project/merge_requests/cherry_pick_changes.html).
Use merge requests for the cherry-picks and resolve any arising conflicts there.
